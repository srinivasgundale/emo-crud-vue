import axios from "axios";
import { authHeader } from "./_helpers/auth-header";
// create a new axios instance
//console.log(authHeader());
const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_ROOT_API
});
axiosInstance.interceptors.request.use(
  function(config) {
    //console.log("request started");
    config.headers.Authorization = authHeader();
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(response => {
  //console.log("request ended");
  return response;
});

export default axiosInstance;