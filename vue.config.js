module.exports = {
    configureWebpack: {
        devtool: 'source-map',
        externals: {
            // global app config object
            config: JSON.stringify({
                apiUrl: "http://127.0.0.1:8000/api/",
            })
        }
    }
}

