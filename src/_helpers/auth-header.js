export function authHeader () {
  // return authorization header with laravel token
  // console.log(authentication.state.user.token);
  let user = JSON.parse(localStorage.getItem('user'))
  // console.log(user.token);

  if (user && user.access_token) {
    return user.access_token
  } else {
    return ''
  }
}
