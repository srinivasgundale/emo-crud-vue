import axios from 'axios'
import { authHeader } from './_helpers/auth-header'
const axiosInstance = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/'
})

axiosInstance.interceptors.request.use(
  function (config) {
    // console.log(authHeader())
    config.headers.Authorization = authHeader()
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(response => {
  // console.log("request ended");
  return response
})

export default axiosInstance
