import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login'
import Signup from '../components/Signup'
import DashboardLayout from '../layouts/DashboardLayout.vue'
import AuthLayout from '../layouts/AuthLayout.vue'
import Articles from '../components/Articles'
import ForgotPassword from '../components/ForgotPassword'
import ResetPassword from '../components/ResetPassword'
import Create from '../components/Create'
import Update from '../components/Update'
Vue.use(Router)
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: AuthLayout,
      redirect: '/signup',
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'signup',
          name: 'Signup',
          component: Signup
        },
        {
          path: 'forgot',
          name: 'ForgotPassword',
          component: ForgotPassword
        },
        {
          path: 'reset/:email',
          name: 'ResetPassword',
          component: ResetPassword
        }
      ]
    },
    {
      path: '/articles',
      component: DashboardLayout,
      redirect: '/articles/list',
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: 'list',
          name: 'Articles',
          component: Articles
        },
        {
          path: 'create',
          name: 'Create',
          component: Create
        },
        {
          path: 'update/:id',
          name: 'Update',
          component: Update
        }
      ]
    }
  ]
})
router.beforeEach((to, from, next) => {
  var currentUser = ''
  currentUser = JSON.parse(localStorage.getItem('user'))
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('/signup')
  else if (!requiresAuth && currentUser) next('/articles')
  else next()
})

export default router
