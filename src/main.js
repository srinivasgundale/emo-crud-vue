// The Vue build version to load with the `import` command
import Vue from 'vue'
import App from './App'
import router from './router'
import Axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2'

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css'

Vue.config.productionTip = false
Vue.prototype.$http = Axios

Vue.use(VueSweetalert2)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
